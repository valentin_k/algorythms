#include <fstream>
#include <deque>
#include <vector>
#include <algorithm>
#include <math.h>
#define MAX  32767
#define LIM 4

using namespace std;

void praefixus (string *s,vector<int> * pi)
{
    int i,j;

    for (i=1;i<pi->size();i++)
    {
        j=(*pi)[i-1];

        for (; j>0 && (*s)[i]!=(*s)[j];j=(*pi)[j-1])
        { }

        if ((*s)[i]==(*s)[j])
        {
            j++;
        }

        (*pi)[i]=j;
    }
}

int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

    string alphabet="abcdefghijklmnopqrstuvwxyz";
    vector<char> c;
    string a,x;
    vector<int> m;
    vector<int> l;

    m.assign(alphabet.size(),0);
    int k=0;
    vector< vector<int> > t;
    in>>x;
    l.assign(x.size(),0);
    in>>a;


    for(int i=0;i<x.size();i++)
    {
        if(m[x[i]-97]==0)
        {
            k++;
            m[x[i]-97]=k;
            c.push_back(x[i]);
        }
    }

    t.resize(x.size());
    for (int i=0;i<x.size();i++)
    {
        t[i].assign(k+1,0);
    }


    praefixus(&x,&l);

    l[0]=0;

    for (int i=0;i<t.size();i++)
    {
        //t[i][0]=0;  //видимо левый символ
        for (int j=1;j<t[i].size();j++)// k+1
        {
            //printf("~ %c %c\n",c[j-1],x[i]);
            if (c[j-1]==x[i])
            {
                //t[i][j]=i+1; // на следующий левел
                t[i][j]=i+1;
                //printf("UP ");

            }

            else
            {
                int ll=l[i-1];
                while(ll>0 && x[ll]!=c[j-1])
                {
                    //printf("~ %c  %c\n",x[ll],c[j-1]);
                    ll=l[ll];
                }
                if (x[ll]==c[j-1])
                {
                    t[i][j]=ll+1;
                    //t[i][j]=9;
                }
                else
                {
                    t[i][j]=0;
                    //t[i][j]=-1;

                }
            }

        }
    }

bool flag=true;



for (int i=0,s=0;i<a.size();i++)
    {
        //printf(" letter %c is # %d\n",a[i],m[a[i]-97] );
        int temp=m[a[i]-97];     //m[a[i]-97];
        s=t[s][temp];
        //printf(" s= %d\n",s );


        if (s==x.size())
        {
            out<< i-x.size()+2;
            flag=false;
            break;
        }
    }
    if (flag)
    {
        out<<-1;
    }

    in.close();
	out.close();

	return 0;
}
