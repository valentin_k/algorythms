#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>


using namespace std;

struct treap
{
        int x;
        int y;
        treap * left;
        treap * right;
        //treap*  up;
};

struct treap * merge(struct treap * l,struct treap * r)
{
    if (l==0)
    {
        return r;
    }
    if (r==0)
    {
        return l;
    }
    if (l->y>r->y)
    {
        l->right=merge(l->right,r);
        return l;
    }
    else
    {
        r->left=merge(l,r->left);
        return r;
    }
}

void split(struct treap * u,int x, struct treap ** l,struct treap ** r)
{

    if (u==0)
    {

        *l=0;
        *r=0;

    }
    else if (x<u->x)
    {
        split(u->left,  x,   l,   &(u->left));
        *r=u;
    }
    else
    {
        split(u->right,  x,   &(u->right)  ,r   );
        *l=u;
    }
}

void add(struct treap **u,int x,int y)
{
    struct treap * v;
    struct treap * l;
    struct treap * r;

    if(*u==0||((*u)->y<=y))
    {
        //printf("ololo\n");

        split(*u,x,&l,&r);
        v=new treap;
        v->x=x;
        v->y=y;
        v->left=l;
        v->right=r;
        *u=v;

    }

    else
    {

        if(x<(*u)->x)
        {
            add(&((*u)->left),x,y);
        }
        else
        {
            add(&((*u)->right),x,y);
        }
    }

}

void del(struct treap **u,int x)
{
    //cout<<"its time for  "<<x<<endl;
    if (*u==0)
    {
    }
    else
    {
        //cout<<"we on   "<<(*u)->x<<endl;
        if (x==(*u)->x)
        {
            //delete(*u);
            *u=merge((*u)->left,(*u)->right);
            //del(u,x);
        }
        else if(x<(*u)->x)
        {
            del( &((*u)->left),x );
        }
        else
        {
            del( &((*u)->right),x );
        }



    }
}

void full_out (struct treap **u,int c,int ch)
{
    //cout<<"its time for  "<<x<<endl;
    if (*u==0)
    {
    }
    else
    {
        cout<<c<<"  "<<ch<<"  "<<(*u)->x<<"  "<<(*u)->y<<endl;
        //cout<<"we on   "<<(*u)->x<<endl;

            full_out(&((*u)->left),c+1,1) ;
            full_out(&((*u)->right),c+1,3) ;
    }
}

int find(struct treap **u,int x,int c)
{
    //printf("%d %d %d\n",x,(*u)->x,c);
    //printf("%d %d  ",x,c);
    if (*u==0)
    {
        //printf("not found\n");
        return -1;
    }
    else
    {
        //printf(" %d\n",(*u)->x);
        if (x==(*u)->x)
        {
            return c;
        }
        else if(x<(*u)->x)
        {
            find( &((*u)->left),x,c+1 );
        }
        else
        {
            find( &((*u)->right),x,c+1 );
        }
    }
}

int main()
{
    string s,sa,sb,s_i,command;

    int a,b,spc;

    //---------------------------------------------------------------------------


    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdout);

    //---------------------------------------------------------------------------
    treap* t=0;

    int f;

    while (true)
    {
            s.clear();
            sa.clear();
            sb.clear();
            command.clear();
            getline(cin,s_i);
            s.insert(0,s_i, 1, s_i.length()-1);
            command.insert(0,s_i, 0, 1);

            if (command=="E")
            {
                break;
            }
            else if(command=="+")
            {
                spc=s.find (" ");

                sa.insert(0,s, 0, spc);
                a=atoi(sa.c_str());

                sb.insert(0,s, spc, s.length()-spc);
                b=atoi(sb.c_str());

                f=find(&t,a,1);
                if (f==-1)
                {
                    add(&t,a,b);
                }
                else
                {
                    //printf("%d ",f);
                }



            }
            else if(command=="-")
            {
                a=atoi(s.c_str());


                del(&t,a);

            }
            else if(command=="?")
            {
                a=atoi(s.c_str());
                f=find(&t,a,1);
                if (f==-1)
                {
                    printf("n ");
                }
                else
                {
                    printf("%d ",f);
                }
            }


    }


    return 0;
}
