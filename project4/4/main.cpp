#include <fstream>
#include <vector>
#include <deque>
#include <math.h>
#define MAX  32767

using namespace std;

class segment_tree
{
    private:
        vector <int> body;
        int siz;

        int sum(int i)
        {
            int sum = body[i+siz];
            for(i+=siz;i>1;i/=2)
            {
                if (i%2==1)
                {
                    sum+=body[2*(i/2)];
                }
            }
            return sum;
        }

    public:
        segment_tree(int n)
        {
            body.resize(30*n);
            siz=n;
        }

        int  s(int u, int v)
        {
            return sum(v)-sum(u-1);
        }

        void c (int k,int d)
        {

            for(k+=siz;k>0;k/=2)
            {
                body[k]+=d;
            }
        }


};


int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

    int  u, v;
    segment_tree tree(300000);
    string cc;


while (!in.eof())
	{
	    in >> cc ;

		if (cc=="C")
		{
            in >> u >>v;
            tree.c(u,v);
		}
		else if (cc=="S")
		{
            in >> u >>v;
            out << tree.s(u,v)<<endl;
		}
		else
		{
		    break;
		}
	}



    in.close();
	out.close();

	return 0;
}
