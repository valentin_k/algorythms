#include <fstream>
#include <deque>
#include <vector>
#include <math.h>
#include <algorithm>

#define MAX  32767

using namespace std;

vector<vector<int> > graph;
vector<vector<int> > dpv;
vector<int > parrent;
vector<int > h;
vector<bool > visited;

int logn;

int up(int u,int v)
{
    if (dpv[u].size()>v)
    {
        return dpv[u][v];
    }

    if (v<=0)
    {
        return parrent[u];
    }
    else
    {
        return up(up(u,v-1),v-1);
    }
}

void dfs(int c,int p,int high)
{
    parrent[c]=p;
    visited[c]=true;
    h[c]=high;

    for (int i=0;i<graph[c].size();i++)
    {
        if (!visited[graph[c][i]])
        {
            dfs(graph[c][i],c,high+1);
        }
    }
}

int lca(int a, int b)
{
    int u,v;

    if (h[a]>h[b])
    {
        v=a;
        u=b;
    }
    else
    {
        u=a;
        v=b;
    }

    for (int i=logn;i>0;i--)
    {
        //printf("W   %d %d  \n",u,v);
        if(h[u]-h[v]>=pow(2,i))
        {
            u=dpv[u][i];
        }
    }

    if (u==v)
    {
        return u;
    }

    for (int i=logn;i>0;i--)
    {
        //printf("Q   %d %d  \n",u,v);
        if(dpv[v][i]!=dpv[u][i])
        {
            v=dpv[v][i];
            u=dpv[u][i];
        }
    }
    //printf("ololo\n");
    return parrent[v];
}

/*
*/

int main()
{
    ifstream in("input.txt");
	ofstream out("output.txt");


	int u,v,n,c;

    in >> n;

    logn = ceil(log(n)/log(2));

    graph.resize(n);
    dpv.resize(n);
    parrent.resize(n);
    h.resize(n);
    visited.assign(n,false);

    for (int i = 0; i < n-1; i++)
	{
		in >> u >> v;
		u--;
		v--;
		graph[u].push_back(v);
        graph[v].push_back(u);
	}

	dfs(0,0,0);

	    for (int i = 0; i < n; i++)
	{
        for (int j=0;j<logn;j++)
        {
            dpv[i].push_back(up(i,j));
            //printf("%d  ",1+up(i,j));
        }
        //printf("\n");
	}

    in >> c;


	for (int i=0;i<c;i++)
    {

        in >> u >> v;
        u--;
        v--;
        //printf("%d %d  ~ %d\n",u,v,1+lca(u-1,v-1));
        out<<1+lca(u,v)<<" ";
    }


	in.close();
	out.close();
	return 0;
}

