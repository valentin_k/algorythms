#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;

struct Node{
    int x, status;
    Node* sib;
};


void action(int i, stack<int> &Stk, Node* A[]){
    Node *p = A[i]->sib; 
    while (p != 0){
        if (p->status == 0){
            p->status = 1;
            action(p->x, Stk, A);
        }
        p = p->sib;
    }
    Stk.push(i);
}

Node *A [100001];

int main(){

    FILE *ofp, *ifp;
    ifp = fopen("input.txt", "r");
    ofp = fopen("output.txt", "w+");

    int n;
    fscanf(ifp, "%d", &n);

    vector<int> T1, T2;
    T1.resize(n+1, 0);
    T2.resize(n+1, 0);

    for (int i = 1; i <= n; ++i){
        int k;
        fscanf(ifp, "%d", &k);
        
        A[i] = new Node;
        A[i]->x = i;
        A[i]->sib = 0;
        
        T1[i] = k;
        for (int j = 1; j <= k; ++j){
            int v;
            fscanf(ifp, "%d", &v);
            
            Node *tmp = new Node;
            tmp->x = v;
            tmp->status = 0;
            tmp->sib = A[i]->sib;
            A[i]->sib = tmp;

            T2[v]++;
        }
    }

    for (int i = 1; i <= n; ++i){
        if (T1[i] != T2[i]){
            fprintf(ofp, "-1");
            return 0;
        }
    }

    stack<int> Stk;
    action(1, Stk, A);

    while(Stk.size() > 0){
        fprintf(ofp, "%d ", Stk.top());
        Stk.pop();
    }

    return 0;
}