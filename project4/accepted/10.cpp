#include <fstream>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>;
#include <iostream>

using namespace std;

int main(){
  
  ifstream ifp;
  ofstream ofp;
  
  ifp.open("input.txt"); 
  ofp.open("output.txt");
  
  string s;
  getline(ifp,s);

  vector<int> A(s.length(), 0);
  
  for (int i = 1; i < s.length(); i++){
    int l = A[i-1];

    while ((l > 0) && (s[l] != s[i])){
      l = A[l-1];
    }

    if (s[l] == s[i]){
      l++;
    }

    A[i] = l;
  }

  for (int i = 0; i < s.length(); ++i){
    ofp << A[i];
  }

  return 0;
}
