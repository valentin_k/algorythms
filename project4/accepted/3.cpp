#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>


using namespace std;


struct Node{
    int x, y;
    Node *l, *r;
};


void split(Node *u, int x, Node **l, Node **r){
    if (u==0){
        *l = 0;
        *r = 0;
    } else if (x < u->x){
        split(u->l, x, l, &(u->l));
        *r = u;
    }else {
        split(u->r, x, &(u->r), r);
        *l = u;
    }
}


void add(Node **u, int x, int y){
    Node *v, *l, *r;

    if(*u==0 || ((*u)->y<=y)){
        split(*u, x, &l, &r);
        v = new Node;
        v->x = x;
        v->y = y;
        v->l = l;
        v->r = r;
        *u = v;
    }else{
        if(x < (*u)->x){
            add(&((*u)->l), x ,y);
        }
        else{
            add(&((*u)->r), x, y);
        }
    }
}


Node* merge(Node *l, Node *r){
    if (l==0){
        return r;
    }
    if (r==0){
        return l;
    }
    if (l->y > r->y){
        l->r = merge(l->r, r);
        return l;
    }
    else{
        r->l = merge(l, r->l);
        return r;
    }
}


void delet(Node **u, int x){
    if (*u != 0){
        if (x == (*u)->x){
            *u=merge((*u)->l, (*u)->r);
        } else if(x < (*u)->x) {
            delet(&((*u)->l), x);
        }else{
            delet(&((*u)->r), x);
        }
    }
}


int find(Node **u, int x, int resutl){
    if (*u==0){
        return -1;
    }else{
        if (x==(*u)->x){
            return resutl;
        }else if(x<(*u)->x){
            find(&((*u)->l), x, resutl+1);
        }else{
            find(&((*u)->r), x, resutl+1);
        }
    }
}


int main(){

    FILE *ofp, *ifp;
    ifp = fopen("input.txt", "r");
    ofp = fopen("output.txt", "w+");

    Node *head = 0;

    char c = 'b';
    while (c != 'E' ){

        fscanf(ifp,"%c", &c);

        if(c=='?') {
            int x;
            fscanf(ifp,"%d\n", &x);
            
            int result;
            result = find(&head,x,1);
            if (result==-1){
                fprintf(ofp, "n ");
            }else{
                fprintf(ofp, "%d ",result);
            }
        }else if(c=='-'){
            int x;
            fscanf(ifp,"%d\n", &x);

            delet(&head, x);
        }else if(c=='+'){
            int x, y;
            fscanf(ifp,"%d %d\n", &x, &y);
           
            int result;
            result = find(&head, x, 1);
            
            if (result == -1){
                add(&head, x, y);
            }
        }
    }

    return 0;
}
