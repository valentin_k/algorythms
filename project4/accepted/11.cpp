#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;


struct Node
{
	int data;
	Node* sib; 
};

Node* A1[100001];
Node* A2[100001];
vector<int> T;
vector<int> S;

void dfs1(int ind, int &t, int n){
	S[ind] = 1;
	Node *v;
	v = A1[ind];
	while (v != 0){
		if (S[v->data] == 0){
			dfs1(v->data, t, n);
		}
		v = v->sib;
	}
	t++;
	T[t] = ind;
}


void dfs2(int ind, int n){
	S[ind] = 1;
	Node *v;
	v = A2[ind];
	while (v != 0){
		if (S[v->data] == 0){
			dfs2(v->data, n);
		}
		v = v->sib;
	}
}


int main(){
	FILE *ofp, *ifp;
	ifp = fopen("input.txt", "r");
  	ofp = fopen("output.txt", "w+");
	
	int n, k, tmp;
	fscanf(ifp, "%d", &n);
	
	T.resize(n+1, 0);

	for(int i=1; i<=n; i++){
		A1[i] = 0;
		A2[i] = 0;
	}


	for(int i=1; i<=n; i++){
		fscanf(ifp, "%d", &k);
		for (int j=1; j<=k; ++j){
			fscanf(ifp, "%d", &tmp);
			Node *v1 = new Node();
			v1->data = tmp;
			v1->sib  = A1[i];
			A1[i] = v1;
			
			Node *v2 = new Node();
			v2->data = i;
			v2->sib  = A2[tmp];
			A2[tmp] = v2;
		}
	}


	S.resize(n+1, 0);

	int t = 0;
	for (int i = 1; i <= n; ++i){
		if (S[i] == 0){
			dfs1(i, t, n);
		}
	}

	for (int j=1; j<=n; ++j){
		S[j] = 0;
	}

	int answer = 0;
	for (int i = t; i >= 1 ; --i){
		int tmp = T[i];
		if (S[tmp] == 0){
			dfs2(tmp, n);
			answer++;
		}
	}

	fprintf(ofp,"%d\n",answer);
    // printf("Answer = %d\n", answer);		
 
	return 0;
}
