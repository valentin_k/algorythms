#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <stack>


using namespace std;


int action(int i, vector<int> A){
    while (A[i] != i){
        i = A[i];
    }
    return i;
}

int main(){

    FILE *ofp, *ifp;
    ifp = fopen("input.txt", "r");
    ofp = fopen("output.txt", "w+");

    int n;
    fscanf(ifp, "%d", &n);

    vector<int> A, B, C;
    A.resize(n+1);
    B.resize(n+1);
    C.resize(n+1);

    for (int i = 1; i <= n; ++i){
        A[i] = i;
        B[i] = 1;
    }

    while (1){
        int u, v;
        if (fscanf(ifp, "%d %d", &u, &v) == EOF){
            break;
        }

        int tmp1, tmp2;
        tmp1 = action(u, A);
        tmp2 = action(v, A);

        if (tmp1 != tmp2){
            if (B[tmp1] > B[tmp2]){
                A[tmp2] = tmp1;
                B[tmp1] = B[tmp1]+ B[tmp2];
            }else{
                A[tmp1] = tmp2;
                B[tmp2] = B[tmp2]+ B[tmp1];
            }
        }
    }

    for (int i = 1; i <= n; ++i){
        C[action(i, A)] = 1;
    }
    
    int k = 0;
    for (int i = 0; i < n; ++i){
        if (C[i] == 1){
            k++;
        }
    }

    fprintf(ofp, "%d\n", k);

    return 0;
}