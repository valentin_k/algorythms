#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>


using namespace std;

struct node{
	string data;
	node *left, *right, *parent;
};

string convertInt(int number){
    if (number == 0)
        return "0";
    string temp="";
    string returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}

void printTree(node* head){
  if (head == 0){
  	return;
  }
  cout<<" data="<<head->data<<endl;
  if (head->right != 0){
  	printTree(head->right);
  }
  if (head->left != 0){
  	printTree(head->left);
  }
}

node* make(string data, node* parent){
	node* newNode = new node;
	newNode->data = data;
	newNode->parent = parent;
	newNode->right = 0;
	newNode->left = 0;
	return newNode;
}


void add(string data, node* head){
	node *v, *u;
	v = head;
	while ((data>v->data && v->right!=0) || (data<v->data && v->left!=0)){
		if (data > v->data){
			v = v->right;
		}else{
			v = v->left;
		}
	}
	if(v->data == data){
		return;
	}
	u = make(data, v);
	if(data < v->data){
		v->left = u;
	}else{
		v->right = u;
	}
}

node* search(int& h, string data, node *head){
	node* v = head;
	if (v == 0){
		return 0;
	}
	while ((data>v->data && v->right!=0) || (data<v->data && v->left!=0)){
		h++;
		if (data > v->data){
			v = v->right;
		} else{
			v = v->left;
		}
	}
	if (v->data == data){
		return v;
	}else{
		return 0;
	}
}

void del(string data, node *head){
 	int h = 1;
 	node *v = search(h, data, head);
 	node *t;
	if (v == 0){
		return;
	}
	if (v==head && v->left==0 && v->right==0){
		head = 0;
		return;
	}
	if (v->left!=0 && v->right!=0){
		t = v->right;
		while (t->left != 0 && v->right!=0){
			t = t->left;
		}
		v->data = t->data;
		v = t;
	}
	if (v->left == 0){
		t = v->right;
	}else{
		t = v->left;
	}
	if (t != 0){
		t->parent = v->parent;
	}
	if (v->parent->left == v){
		v->parent->left = t;
	}else{
		v->parent->right = t;
	}
}

int main(){
	ifstream ifp("input.txt");
	ofstream ofp("output.txt");
	string s="", answer = "";
	node *tmp, *head = 0;
	bool isHead = false;
	while (s[0] != 'E'){
		getline(ifp, s);
		if (s[0] == '+'){
			if (isHead == false){
				head = make(s.substr(1, s.length()), head);
				isHead = true;
			}else{
  				add(s.substr(1, s.length()), head);
			}
		}else if (s[0] == '-'){
  			del(s.substr(1, s.length()), head);
  		}else if (s[0] == '?'){
  			int h =1;
  			tmp = search(h, s.substr(1, s.length()), head);
  			if (tmp == 0){
  				answer+='n';
  			}else{
  				answer+=convertInt(h);
  			}
  		}
	}
	ofp << answer;
	ifp.close();
  	ofp.close();

	return 0;
}
