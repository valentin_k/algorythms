#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

void change(int k, int d, int A[]){
    while (k > 0){
        A[k] += d;
        k /= 2;
    }
}

int sum(int b, int A[]){
    int s = A[b];
    while (b > 1){
        if (b % 2 == 1){
            s += A[2 * (b / 2) ];
        }
        b /= 2;
    }
    return s;
}

int n =300000;
int A[9000000];

int main(){
    ifstream in("input.txt");
    ofstream out("output.txt");

    while (1){   

        char c;     
        in >> c;
    
        if (c == 'E'){
            break;
        }
    
        int k, d;
        if (c == 'C'){
            in >> k >> d;
            change(n+k, d, A);
        }

        int a, b;
        if (c == 'S'){
            in >> a >> b;
            out << sum(n + b, A) - sum(n + a - 1, A) << endl;
        }
    }

    in.close();
    out.close();

    return 0;
}
