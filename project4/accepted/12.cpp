#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>


using namespace std;


struct Node{
    Node *sib;
    int x; 
};

void dfs(FILE *ofp, int i, int &n, int S[], int Answer[], Node *A[]){
    Node *v;
    S[i] = 1;
    v = A[i];
    while (v != 0){
        if (S[v->x] == 1){
            fprintf(ofp, "-1");
            exit(0);
        }
        if (S[v->x]==0){
            dfs(ofp, v->x, n, S, Answer, A);
        }
        v = v->sib;
    }
    n++;
    Answer[n] = i;
    S[i] = 2;
}

Node *A[1000001];
int S[1000001], Answer[1000001];


int main(){

    FILE *ofp, *ifp;
    ifp = fopen("input.txt", "r");
    ofp = fopen("output.txt", "w+");

    int n, m;
    fscanf(ifp, "%d %d", &n, &m);

    for (int i = 1; i <= n; ++i){
        A[i] = 0;
        S[i] = 0;
        Answer[i] = 0;
    }

    for (int i = 1; i <= m; ++i){
        int u, v;
        fscanf(ifp, "%d %d", &u, &v);
        Node *tmp = new Node;
        tmp->x = v;
        tmp->sib = A[u];
        A[u] = tmp;
    }

    int k = 0;
    for (int i = 1; i <= n; ++i){
        if (S[i] == 0){
            dfs(ofp, i, k, S, Answer, A);
        }
    }

    if (k ==0){
        fprintf(ofp, "-1");
        return 0;
    }

    for (int i = k; i > 0; --i){
        fprintf(ofp, "%d ", Answer[i]);
    }

    return 0;
}