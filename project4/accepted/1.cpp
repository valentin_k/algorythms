#include <string>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>


using namespace std;

struct Rib{
	int sib;
	int dist;
};

int main(){
	FILE *ofp, *ifp;
	ifp = fopen("input.txt", "r");
	ofp = fopen("output.txt", "w+");

	int n, m;
	fscanf(ifp, "%d %d", &n, &m);

	vector<Rib> A[100001];
	
	for (int i = 0; i < m; i++){
		int  u, v, dist;
		fscanf(ifp, "%d %d %d", &u, &v, &dist);

		Rib obj1;
		obj1.sib = v;
		obj1.dist = dist;
		A[u].push_back(obj1);

		Rib obj2;
		obj2.sib = u;
		obj2.dist = dist;
		A[v].push_back(obj2);
	}

	int D[100001];
	for (int i = 2; i <= n; i++){
		D[i] = 100001;
	}		
	D[1] = 0;

		
	vector<int> queue;
	queue.push_back(1);
	
	while (!queue.empty()){
		int tmp;
		tmp = queue[0];
		queue.erase(queue.begin());
		
		for (int i = 0; i < A[tmp].size(); i++){
			int sib, dist;
			sib = A[tmp][i].sib;
			dist = A[tmp][i].dist;

			if (D[sib] > D[tmp] + dist){
				D[sib] = D[tmp] + dist;
				if (dist != 0){
					queue.push_back(sib);
				} else{
					queue.insert(queue.begin(), sib);
				}
			}
		}
	}

	if (D[n] != 100001){
		fprintf(ofp, "%d\n", D[n]);
		// printf("%d\n", D[n]);		
	}else{
		fprintf(ofp,"-1\n");
		// printf("-1\n");	
	}

	return 0;
}