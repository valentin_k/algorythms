#include <fstream>
#include <vector>
#include <algorithm>
#include <utility>

#define MAX  32767

using namespace std;

class graph
{
    private:
            vector <vector <int> > g;
            vector <bool> used;
            vector <bool> bridge_end;
            vector <int> tin;
            vector <int> fup;
            vector <int> biconnected;
            vector <vector <int> > bridges;
            vector <int> neighbors;
            int timer;
            void dfs (int u, int p=-1)
            {
                //printf("dfs %d %d\n",v+1,p+1);
                used[u]=true;
                timer++;
                fup[u]=timer;
                tin[u]=timer;
                int children=0;
                for (int v=0;v<g[u].size();v++)
                {
                    int to=g[u][v];

                    if (to == p)  continue;
                    if (used[to])
                        fup[u] = min (fup[u], tin[to]);
                    else
                    {
                        dfs (to, u);
                        fup[u] = min (fup[u], fup[to]);
                        if (fup[to] >= tin[u] && p != -1)
                        {
                            //printf("%d  \n",v+1);
                            biconnected.push_back(u);
                        }
                        if (fup[to] > tin[u])
                        {
                            bridges[min(u,to)].push_back(max(u,to));
                            bridge_end[u]=true;
                            bridge_end[to]=true;
                        }
                        children++;
                    }
                }

                if (p == -1 && children > 1)
                {
                    biconnected.push_back(u);
                    //printf("%d  \n",v+1);
                }

              }

              bool bridge_test(int a, int b)
              {
                    if (!bridge_end[a]||!bridge_end[b])
                    {
                        return false;
                    }
                    else
                    {
                        int u=min(a,b);
                        int v=max(a,b);

                        for (int i=0;i<bridges[u].size();i++)
                        {
                            if (bridges[u][i]==v)
                            {
                                return true;
                            }
                            else if (bridges[u][i]>v)
                            {
                                return false;
                            }

                        }

                        return false;
                    }
              }

            void dfs2 (int u,int p)
            {
                neighbors.push_back(u);
                //printf("got into %d   \n",u+1);
                used[u]=true;
                //printf("test   ");
                for (int v=0;v<g[u].size();v++)
                {
                    int to=g[u][v];

                      //printf("%d  ",to+1);

                    //pair<int,int> temp(min(v,to) ,max(v,to));
                    //printf("~ololo   %d %d %d",used[to], to == p, bridge_test(u,to) );
                    if (used[to] || to == p|| bridge_test(u,to) )  continue;
                    else
                    {
                    //printf("\n");
                        dfs2 (to,u);
                    }
                }

              }
    public:
            vector <int> result;
            graph(int n)
            {
                g.resize(n);
                bridges.resize(n);
                used.assign(n,false);
                bridge_end.assign(n,false);
                tin.assign(n,MAX);
                fup.assign(n,MAX);
                timer=0;
            }
            void add(int u,int v)
            {
                g[u].push_back(v);
                g[v].push_back(u);
            }
              void output()
              {

                for (int i=0;i<used.size();i++)
                {
                    if (!used[i])
                    {
                        dfs(i,-1);
                    }
                }

                if (biconnected.size()>0)
                {
                    biconnected.push_back(biconnected[0]);
                    sort(biconnected.begin(),biconnected.end());
                    for (int i=0;i<bridges.size();i++)
                    {
                        if (bridges[i].size()>0)
                        {
                            sort(bridges[i].begin(),bridges[i].end());
                        }
                    }

                    //printf("%d ",biconnected[0]+1);

                        result.push_back(biconnected[0]+1);
                }


                for (int i=1;i<biconnected.size();i++)
                {
                    if(biconnected[i]!=biconnected[i-1])
                    {
                        //printf("%d ",biconnected[i]+1);
                        result.push_back(biconnected[i]+1);
                    }
                }

                //printf("\n");
                for (int i=0;i<bridges.size();i++)
                {
                    for (int j=0;j<bridges[i].size();j++)
                    {
                        result.push_back(1+i);
                        result.push_back(1+bridges[i][j]);
                    }
                }
                used.assign(g.size(),false);
                dfs2(0,-1);
                sort(neighbors.begin(),neighbors.end());
                //printf("\n");
                for (int i=0;i<neighbors.size();i++)
                {
                    //printf("%d ",1+neighbors[i]);
                    result.push_back(1+neighbors[i]);
                }


              }

};

int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

    int n,u,v;

	in >> n ;

    graph g(n);

while (!in.eof())
	{

		in >> u >> v;
        if(in.eof())
        {
            break;
        }
		u--;
		v--;

		g.add(u,v);

	}

    g.output();

    for (int i=0;i<g.result.size();i++)
    {
        out<<g.result[i]<<" ";
    }


    in.close();
	out.close();

	return 0;
}
