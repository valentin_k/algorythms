#include <fstream>
#include <deque>
#include <vector>
#include <algorithm>

#define MAX  32767

using namespace std;



  class Edge {
      private:
    int s, t, rev, cap, f;

    public:
     Edge(int ss, int tt, int rrev, int ccap) {
      s = s;
      t = tt;
      rev = rrev;
      cap = ccap;
    }
  };


  void addEdge(vector<vector<Edge> > *graph, int s, int t, int cap)
  {
    ((*graph)[s]).push_back(new Edge(s, t, (*graph)[t].size(), cap));
    (*graph)[t].push_back(new Edge(t, s, graph[s].size() - 1, 0));
  }

  public static int maxFlow(List<Edge>[] graph, int s, int t) {
    int flow = 0;
    int[] q = new int[graph.length];
    while (true) {
      int qt = 0;
      q[qt++] = s;
      Edge[] pred = new Edge[graph.length];
      for (int qh = 0; qh < qt && pred[t] == null; qh++) {
        int cur = q[qh];
        for (Edge e : graph[cur]) {
          if (pred[e.t] == null && e.cap > e.f) {
            pred[e.t] = e;
            q[qt++] = e.t;
          }
        }
      }
      if (pred[t] == null)
        break;
      int df = Integer.MAX_VALUE;
      for (int u = t; u != s; u = pred[u].s)
        df = Math.min(df, pred[u].cap - pred[u].f);
      for (int u = t; u != s; u = pred[u].s) {
        pred[u].f += df;
        graph[pred[u].t].get(pred[u].rev).f -= df;
      }
      flow += df;
    }
    return flow;
  }


int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

    int n, m, u, v, d,i,j,s;

	in >> n ;


while (!in.eof())
	{
		in >> u >> v >>d;
		u--;
		v--;

        if(in.eof())
        {
            break;
        }

	}


    printf("%d\n\n",s);


    in.close();
	out.close();

	return 0;
}
