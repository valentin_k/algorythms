#include <fstream>
#include <deque>
#include <vector>

#define MAX  32767

using namespace std;

struct node
{
    int direction;
    int weight;
};

int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

	deque<int> queue;
    int *distance;
	int n, m, u, v, d, tmp, tmp1,i;
	node temp;

	in >> n >> m;

	vector<vector<node> > graph(n);


	for (i=0; i<m; i++)
	{
		in >> u >> v >>d;
		u--;
		v--;

		temp.direction=u;
		temp.weight=d;

		(graph[v]).push_back(temp);

        temp.direction=v;

		graph[u].push_back(temp);
	}

	for (int i = 0; i <n; i++)
	{
	    distance[i]=MAX;
	}

	queue.push_back(1);
	distance[0] = 0;

	while (!queue.empty())
	{
		v = queue.front();
		queue.pop_front();

		for (i=0; i < int(graph[v].size()); i++)
		{
			if (distance[graph[v][i].direction]>distance[v]+graph[v][i].weight)
			{
				distance[graph[v][i].direction] = distance[v]+graph[v][i].weight;
				if (graph[v][i].weight==0)
				{
				    queue.push_front(graph[v][i].direction);
				}
				else
				{
				    queue.push_back(graph[v][i].direction);
				}
			}
		}
	}

	if (distance[n-1]==MAX)
	{
	    out << -1;
	}
	else
    {
	    out << distance[n-1];
	}

	in.close();
	out.close();

	return 0;
}
