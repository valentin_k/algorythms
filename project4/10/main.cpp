#include <fstream>
#include <vector>
#include <string>

#define MAX  32767

using namespace std;

void praefixus (string *s,vector<int> * pi)
{
    int i,j;

    for (i=1;i<pi->size();i++)
    {
        j=(*pi)[i-1];

        for (; j>0 && (*s)[i]!=(*s)[j];j=(*pi)[j-1])
        { }

        if ((*s)[i]==(*s)[j])
        {
            j++;
        }

        (*pi)[i]=j;
    }


}

int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

	string s;
	int i;

	in >> s;

	vector<int> pi(s.size());

	praefixus(&s, &pi);

    for (i=0;i<pi.size();i++)
    {
        out <<pi[i]<<" ";
    }

	in.close();
	out.close();

	return 0;
}
