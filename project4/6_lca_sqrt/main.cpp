#include <fstream>
#include <deque>
#include <vector>
#include <math.h>
#include <algorithm>

#define MAX  32767

using namespace std;


int minimal(vector<int> * v,vector<int> * h)
{
    int cur_i, cur_v,i;

/*
    for (i=0;i<v->size();i++)
	{
		printf("!! %d %d \n",1+(*v)[i], (*h)[(*v)[i]] );
	}

	printf(" \n");
*/
    if (v->size()==2)
    {
    	if ( (*h)[ (*v)[0] ] <  (*h)[ (*v)[1] ] )
    	{
    		return (*v)[0] ;
    	}
    	else
    	{
    		return (*v)[1] ;
    	}
    }

    for (cur_i=(*v)[0],cur_v=(*h)[(*v)[0]],i=0;i<=v->size();i++)
    {
        if (cur_v>(*h)[(*v)[i]])
        {
            cur_v=(*h)[(*v)[i]];
            cur_i=(*v)[i];
        }
    }

    return cur_i;
}

int minimal(vector<int> * ord,vector<int> * h, int a,int b)
{
    int i;
    int tmp;
    int tmp_node;

    if (a>b)
    {
        tmp=a;
        a=b;
        b=tmp;

    }


    if (a>ord->size()-1)
    {
        a=ord->size()-1;
    }

    if (b>ord->size()-1)
    {
        b=ord->size()-1;
    }

    //printf("%d  - ~- %d\n",a,b);

//printf("ololo %d \n",(*h)[(* ord)[b]]);
    for (tmp=(*h)[(* ord)[b]],tmp_node=(* ord)[b],i=a;i<b;i++)
    {
        //printf("ololo %d \n",(*h)[(* ord)[i]]);
        if (tmp>(*h)[(*ord)[i]])
        {
            tmp=(*h)[(*ord)[i]];
            tmp_node=(* ord)[i];
        }
    }

    return tmp_node;
}

vector<int> keygen(vector<int> * ord,vector<int> * h)
{
    int i,k;


    k=(int)(0.5+sqrt(2*(h->size()-1) ));

    vector<int> key(k+1,MAX);

   for(i=0;i<=k;i++)
   {

        key[i]=minimal(ord,h,i*k,(i+1)*k-1);

   }

    return key;
}

void dfs(vector<vector<int> > *tree, int current, int father,int depth,vector<int> * result,vector<int> * h,vector<int> * first_entrance)
{
    int i,n;
    n= (*tree)[current].size();

    //printf("%d \n", 1+current);
    result->push_back(current);
    (*h)[current]=depth;

    if ((*first_entrance)[current]<0)
    {
        (*first_entrance)[current]=result->size() -1;
    }

    for (i=0;i<n;i++)
    {
        if ( (*tree)[current][i]!=father)
        {
            dfs(tree,(*tree)[current][i],current,depth+1,result,h,first_entrance);
            result->push_back(current);
            //printf("%d \n",1+ current);
        }
    }


}


int query(vector <int> * first_entrance, vector<int> * ord,vector<int> * h,vector<int> * key,int u,int v)
{
    int temp,k;


    if (u==v)
    {
        //printf("%d %d ~~ %d \n",u,v,1+u);
        return u;
    }
    else
    {
        k=key->size()-1;

            //printf("~ %d %d %d\n",u,v,k);
            u=(*first_entrance ) [u];
            v=(*first_entrance ) [v];


            if (u>v)
            {
            	temp=u;
            	u=v;
            	v=temp;
            }



        if (v-u<k)
        {
            //printf("~ %d %d %d\n",u,v,k);
            temp=minimal(ord,h,u,v);
            //printf("@ %d \n",1+temp);
            return temp;
            //
        }
        else
        {

            int a,b,i;
            vector <int> candidates;

            a=ceil(((float)u)/k);
            b=floor(((float)v)/k);

            //printf("~ %d %d - %d %d - %d\n",u,v,a,b,k);

            for (i=a;i<b;i++)
            {
                candidates.push_back((*key)[i]);
                //printf("A pushed %d\n",1+(*key)[i]);
            }

            for (i=u;i%k>0;i++)
            {
                candidates.push_back((*ord)[i]);
                //printf("B pushed %d\n",1+(*ord)[i]);
            }

            for (i=v;i>=k*(b);i--)
            {
                candidates.push_back((*ord)[i]);
                //printf("c pushed %d\n",1+(*ord)[i]);
            }


            return minimal(&candidates,h);



        }


    }

}

int main()
{
    ifstream in("input.txt");
	ofstream out("output.txt");

	int i,n,k,u,v,a,b,cc;
	vector<int> ord;


    in >> n;

	vector<vector<int> > tree(n);
	vector<int> h(n);
	vector<int> first_entrance(n,-1);


    for (int i = 0; i < n-1; i++)
	{
		in >> u >> v;
		//printf("%d %d\n",u,v);

		tree[u-1].push_back(v-1);
		tree[v-1].push_back(u-1);

	}

	dfs(&tree,0,-1,0,&ord,&h,&first_entrance);


/*
	for (i=0;i<ord.size();i++)
	{
	    //printf("%d %d %d\n",ord[i],h[ord[i]],first_entrance[ord[i]]);
	    printf("%d ",1+ord[i]);
	}
	printf("\n %d \n",ord.size());
*/
    vector<int> key=keygen(&ord, &h);
 /*
    printf("\n ");
	for(i=0;i<key.size();i++)
	{
		printf("%d ",1+key[i]);
	}
	printf("\n ");

    for (i=0;i<key.size();i++)
	{
	    //printf("%d \n",key[i]);
	}
*/
    in >> cc;


	for (i=0;i<cc;i++)
    {
        in >> u >> v;
        //printf("\n -----   %d %d  --- \n",u,v);
        a=query(&first_entrance,&ord,&h,&key,u-1,v-1);
        out<<1+a<<endl;
        //printf("%d ",1+a);
    }

	in.close();
	out.close();
	return 0;
}

