#include <fstream>
#include <vector>
#include <algorithm>
#include <utility>

#define MAX  32767

using namespace std;

class graph
{
    private:
            vector <vector <int > > d;
            vector <vector <int> > p;
            int n;
            void floyd()
            {
                    for (int i=0;i<n;i++)
                    {
                        for (int j=0;j<n;j++)
                        {
                            if (i!=j&&d[i][j]!=MAX)
                            {
                                p[i][j]=i;
                            }
                            else
                            {
                                p[i][j]=0;
                            }
                        }
                    }

                    for (int k=0;k<n;k++)
                    {
                        for (int i=0;i<n;i++)
                        {
                            for (int j=0;j<n;j++)
                            {
                                d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
                            }
                        }
                    }


            }

    public:
            vector <int> result;
            graph(int nn)
            {
                n=nn;
                d.resize(n);
                p.resize(n);
                for (int i=0;i<n;i++)
                {
                    p[i].assign(n,-1);
                    d[i].assign(n,MAX);
                }

                for (int i=0;i<n;i++)
                {
                    for (int j=0;j<n;j++)
                    {
                        if (i==j)
                        {
                            d[i][j]=0;
                        }
                    }
                }
            }
            void add(int u,int v,int w)
            {
                d[u][v]=w;
                d[v][u]=w;
            }
            void output()
            {
                floyd();

                for (int i=0;i<n;i++)
                {
                    for (int j=0;j<n;j++)
                    {
                        //printf("%d  ",p[i][j]);
                        if (d[i][j]<MAX)
                        {
                            result.push_back(d[i][j]);
                        }
                        else
                        {
                            result.push_back(-1);
                        }
                    }
                    //printf("\n");
                }
                //printf("\n");

                for (int i=0;i<n;i++)
                {
                    for (int j=0;j<n;j++)
                    {
                        result.push_back(p[i][j]);
                        //printf("%d  ",d[i][j]);
                    }
                    //printf("\n");
                }
            }
};


int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

    int n,u,v,d;

	in >> n ;


    graph g(n);

while (!in.eof())
	{

		in >> u >> v>> d;
        if(in.eof())
        {
            break;
        }
		u--;
		v--;
        g.add(u,v,d);
		//printf("%d %d\n",u+1,v+1);

	}

g.output();

for (int i=0;i<g.result.size();i++)
{
    out<<g.result[i]<<" ";
}


    in.close();
	out.close();

	return 0;
}
