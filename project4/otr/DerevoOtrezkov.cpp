#include<fstream>

using namespace std;

int n = 600000;
int tree[2000000] = { 0 };

void change(int u, int d);
int sum(int u);

int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");
	char c;
	int a, b;

	while (1)
	{
		in >> c;
		if (c == 'E')
			break;
		if (c == 'C')
		{
			in >> a >> b;
			change(n+a, b);
			
		}
		if (c == 'S')
		{
			in >> a >> b;
			out << sum(n + b) - sum(n + a - 1) << endl;
			
		}
	}

	in.close();
	out.close();
	return 0;
}

void change(int u, int d)
{
	while (u > 0)
	{
		tree[u] += d;
		u /= 2;
	}
}

int sum(int u)
{
	int s = tree[u];
	while (u > 1)
	{
		if (u % 2 == 1)
			s += tree[2 * (u / 2) ];
		u /= 2;
	}
	return s;
}