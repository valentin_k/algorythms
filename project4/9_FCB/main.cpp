#include <fstream>
#include <deque>
#include <vector>
#include <math.h>

#define MAX  32767

using namespace std;


void dfs(vector<vector<int> > *tree, int current, int father,int depth,vector<int> * result,vector<int> * h,vector<int> * first_entrance)
{
    int i,n;
    n= (*tree)[current].size();

    //printf("%d \n", 1+current);
    result->push_back(current);
    (*h)[current]=depth;

    if ((*first_entrance)[current]<0)
    {
        (*first_entrance)[current]=result->size() -1;
    }

    for (i=0;i<n;i++)
    {
        if ( (*tree)[current][i]!=father)
        {
            dfs(tree,(*tree)[current][i],current,depth+1,result,h,first_entrance);
            result->push_back(current);
            //printf("%d \n",1+ current);
        }
    }


}



void st_generator(vector<vector<int> > *table, vector<int> *ord,vector <int> * h)
{
    int i,j,k,n;
    n=ord->size();

    table->resize(n);

    for (i=0;i<n;i++)
    {
        (*table)[i].resize(1);
        (*table)[i][0]=i;
    }

    for(j=1;1<<j <=n ;j++){
        for(i=0;i+(1<<(j-1))<n;i++){
            if( (*h)[(*ord)[(*table)[i][j-1]]]<=(*h)[(*ord)[(*table)[i+(1<<(j-1))]   [j-1] ] ])
                (*table)[i].push_back( (*table)[i][j-1] );
            else
                (*table)[i].push_back(  (*table)[i+(1<<(j-1))][j-1] );
        }
    }


}


int lca(int u,int v,vector <int> * fc,   vector<vector<int> > *t,vector<int> *ord,vector <int> * h)
{
    int temp,k;

    if (u==v)
    {
        return u;
    }

    u=(*fc)[u];
    v=(*fc)[v];



    if (u>v)
    {
        temp=u;
        u=v;
        v=temp;
    }



    //k=log(v-u+1)/log(2);
    k=0;

    while (pow(2,k+1)<=v+1-u)
    {
        k++;
    }

     //printf("found on %d %d   | k= %d\n",u,v,k);

     //printf("t1: %d h: %d  \n",(*ord)[(*t)[u][k] ] ,(*h)[(*ord)[ (*t)[u][k] ]  ]);
     //printf("t1: %d h: %d  \n",(*ord)[(*t)[u][k] ] ,(*h)[(*ord)[ (*t)[u][k] ]  ]);

     //printf("t2: %d h: %d  \n",(*ord)[(*t)[v-(1<<k)][k] ]  , (*h)[(*ord)[(*t)[v-(1<<k)][k] ]  ]     );
     //printf("t2: %d  \n",v-(1<<k) +1    );


    if (   (*h)[(*ord)[ (*t)[u][k] ]  ] <=  (*h)[(*ord)[(*t)[v-(1<<k)+1][k] ]  ]     )
    {
        return  (*ord)[(*t)[u][k] ]  ;
    }
    else
    {
        return  (*ord)[(*t)[v-(1<<k)+1][k] ]  ;
    }


    return 0;

}
/*
int RMQ(int A[],int s,int e){
    int k=e-s;

    //s<e
    k=31-__builtin_clz(k+1); // k = log(e-s+1)
    if(A[M[s][k]]<=A[M[e-(1<<k)+1][k]])
        return A[M[s][k]];
    return A[M[e-(1<<k)+1][k]];
}
*/
int main()
{
    ifstream in("input.txt");
    ofstream out("output.txt");

	int i,n,k,u,v,a,b,cc,j;
	vector<int> ord;


    in >> n;

	vector<vector<int> > tree(n);
	//vector<vector<int> > t(2*n);
	vector<vector<int> > table;
	vector<int> h(n);
	vector<int> first_entrance(n,-1);


    for (int i = 0; i < n-1; i++)
	{
		in >> u >> v;

		tree[u-1].push_back(v-1);
		tree[v-1].push_back(u-1);

	}

	dfs(&tree,0,-1,0,&ord,&h,&first_entrance);


 /*
	for (i=0;i<ord.size();i++)
	{
	    //printf("%d %d %d\n",ord[i],h[ord[i]],first_entrance[ord[i]]);
	    printf("%d ",1+ord[i]);
	}
	printf("\n %d \n",ord.size());
 */

    st_generator(&table,&ord,&h);

    in >> cc;
/*
    for (i=0;i<table.size();i++)
    {
        for (k=0;k<(table[i]).size();k++ )
        {
            printf("a = %d  b= %d  i= %d  j= %d  |    # %d   u %d  h %d  \n",i,i+(int)(pow(2,k)) -1,i,k,table[i][k],1+ord[table[i][k]],h[ord[table[i][k]]]);
            	for (j=i;j<=i+(int)(pow(2,k)) -1;j++)
                {
                    printf("%d ",1+ord[j]);
                }
            printf("\n");
        }
    }
*/
    for (i=0;i<cc;i++)
    {
        in >> u >> v;
        //printf("  %d  ~~~ %d  ",u,v);
        a=lca(u-1,v-1,&first_entrance,   &table,&ord,&h    );
        out<<1+a<<endl;

    }

	in.close();
	out.close();
	return 0;
}
