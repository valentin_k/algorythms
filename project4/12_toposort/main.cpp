#include <fstream>
#include <algorithm>
#include <vector>
#include <deque>
#include <math.h>

#define MAX  32767

using namespace std;

class graph
{
	private:
		vector <bool> used;
		vector <char> color;
		vector < vector <int> > body;
		bool dfs (int i)
		{
            //printf("%d   ",1+i);
			int j;
			bool t=true;
			used[i]=true;
			color[i]='g';
			for (j=0;j<body[i].size();j++)
			{
				if (color[body[i][j]]!='g')
				{
					if (!used[body[i][j]])
					{
						t=dfs(body[i][j]);
						if (!t) return false;
					}
				}
				else
				{
                    //printf("top kek %d   ",1+body[i][j]);
					return false;
				}
			}
			color[i]='b';
			ans.push_back(i);
			return t;
		}
	public:
		vector <int> ans;
		graph(int n)
		{
			used.assign(n,false);
			body.resize(n);
			color.resize(n);
		}
		void add_edge(int u,int v)
		{
			body[u].push_back(v);
		}
		bool toposort()
		{
			int i;
			int n=used.size();
			bool t=true;

			for (i=0;i<n;i++)
			{
				if (!used[i])
				{
                    //printf("gogogo\n");
					color.assign(used.size(),'w');
					t=dfs(i);
					if (!t) return t;
				}
			}
			reverse(ans.begin(), ans.end());
			return t;
		}
};

int main()
{
    ifstream in("input.txt");
    ofstream out("output.txt");

	int i,j,n,m,u,v;
	bool b;

    in>>n>>m;

    graph g(n);


    for (i = 0; i < m; i++)
	{
		in >> u >> v;
		u--;
		v--;
		g.add_edge(u,v);

	}

	b=g.toposort();

	if (b)
	{
			for (i=0;i<g.ans.size();i++)
			{
				out<<1+g.ans[i]<<" ";
			}
	}
	else
	{
		out<<"-1";
	}





        in.close();
        out.close();
        return 0;

}
