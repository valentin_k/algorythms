#include <fstream>
#include <deque>
#include <vector>
#include <algorithm>

#define MAX  32767

using namespace std;


struct edge
{
    int u;
    int v;
    int weight;
};


    bool customsort(edge a,edge b)
    {
        return  a.weight < b.weight;
    }

class dsu
{
    private:
        vector <int> parent;
        vector <int> rank;
        int l;
    public:

        dsu()
        {
            l=0;
        }

        void make_set (int v)
        {
            if (l<=v)
            {
                parent.resize(v+1);
                rank.resize(v+1);
                l=v+1;
            }
            parent[v] = v;
            rank[v] = 0;
        }

        int find_set (int v)
        {
            if (v == parent[v])
            {
                    return v;
            }
            else
            {
                return parent[v] = find_set (parent[v]);
            }
        }

        void union_sets (int a, int b)
        {
            int t;

            a = find_set (a);
            b = find_set (b);

            if (a != b)
            {
                if (rank[a] < rank[b])
                 {
                     t=a;
                     a=b;
                     b=t;
                 }

                parent[b] = a;

                if (rank[a] == rank[b])
                {
                    rank[a]+=1;
                }
            }
}

};


int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");

    int n, m, u, v, d,i,s;

    vector <edge> edges;
    vector <edge> answer;
    edge temp;
    dsu set;

	in >> n ;


while (!in.eof())
	{


		in >> u >> v >>d;
		u--;
		v--;

        if(in.eof())
        {
            break;
        }

        temp.u=u;
        temp.v=v;
        temp.weight=d;

        edges.push_back(temp);

	}

    sort(edges.begin(), edges.end(), customsort);

    for (i=0;i<n;i++)
    {
        set.make_set(i);
    }

    for (i=0,s=0;i<edges.size();i++)
    {
        u=set.find_set(edges[i].u);
        v=set.find_set(edges[i].v);
        if (u!=v )
        {
            set.union_sets(u,v);
            answer.push_back(edges[i]);
            s+=edges[i].weight;
        }
    }

    out<<s<<endl;

    for (i=0;i<answer.size();i++)
    {
        out <<answer[i].u+1<<" "<<answer[i].v+1<<endl;
    }


    in.close();
	out.close();

	return 0;
}
