#include <fstream>
#include <vector>
#include <deque>
#include <math.h>

#define MAX  32767

using namespace std;

struct edge
{
    edge * u;
    int v;
};

int main()
{
    ifstream in("input.txt");
    ofstream out("output.txt");

	int i,j,n,m,u,v;

    in >> n>>m;

    vector<int> answer(n,0);
    vector<int> s(n,0);
    vector<edge> graph(m);


    for (i = 0; i < m; i++)
	{
		in >> u >> v;
		u--;
		v--;
		graph[u].push_back(v);
	}



        in.close();
        out.close();
        return 0;

}
