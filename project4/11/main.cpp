#include <fstream>
#include <deque>
#include <vector>
#include <math.h>
#include <algorithm>
#include <utility>
#define MAX  32767

using namespace std;

vector<vector<int> > graph;
vector<vector<int> > antigraph;
vector<bool >used;


deque< int> timer;

int t=0;

void dfs( int current)
{
    used[current]=true;

    for (int i=0;i<graph[current].size();i++)
    {
        if (!used[graph[current][i]])
        {
            dfs(graph[current][i]);
        }
    }
    timer.push_front(current);
    t++;
}

void adfs( int current)
{
    used[current]=true;
//printf("hi there %d\n",current+1);
    for (int i=0;i<antigraph[current].size();i++)
    {
        if (!used[antigraph[current][i]])
        {
            adfs(antigraph[current][i]);
        }
    }
}

int main()
{
    ifstream in("input.txt");
	ofstream out("output.txt");
	int n,u,v,k;

    in >> n;

	graph.resize(n);
	antigraph.resize(n);
	used.assign(n,false);

    for (u=0; u<n; u++)
	{
		in >> k;
        //counter[u]+=k;

        for (int j=0;j<k;j++)
        {
            in >> v;
            v--;

            graph[u].push_back(v);
            antigraph[v].push_back(u);
        }

	}

	for (int i=0;i<n;i++)
	{
        if (!used[i])
        {
            //printf("~%d",1+i);
            dfs(i);
        }
	}
/*
	for (int i=0;i<timer.size();i++)
	{
        printf("%d \n",1+timer[i]);
	}
*/
    int c=0;
    used.assign(n,false);
//printf("\n~~\n",k);

	for (int i=0;i<timer.size();i++)
	{

            if (!used[timer[i]])
        {
        //printf("ololo  %d\n",1+timer[i]);
            c++;
            adfs(timer[i]);
        }
	}

//printf("\n%d \n",c);

out<<c;


	in.close();
	out.close();
	return 0;
}


