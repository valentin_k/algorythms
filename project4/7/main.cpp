#include <fstream>
#include <deque>
#include <vector>
#include <math.h>

#define MAX  32767

using namespace std;


class segment_tree
{
    private:
        vector <int> body;
        int siz;

        int sum(int i)
        {
            int sum = body[i+siz];
            for(i+=siz;i>1;i/=2)
            {
                if (i%2==1)
                {
                    sum+=body[2*(i/2)];
                }
            }
            return sum;
        }

    public:
        segment_tree(int n)
        {
            body.resize(30*n);
            siz=n;
        }

        int  s(int u, int v)
        {
            return sum(v)-sum(u-1);
        }

        void c (int k,int d)
        {

            for(k+=siz;k>0;k/=2)
            {
                body[k]+=d;
            }
        }


};


int main()
{
    ifstream in("input.txt");
    ofstream out("output.txt");


	int i,n,u,v,cc;
	vector<int> ord;


    in >> n;

	vector<vector<int> > tree(n);
	vector<int> h(n);
	vector<int> first_entrance(n,-1);


    for (int i = 0; i < n-1; i++)
	{
		in >> u >> v;

		tree[u-1].push_back(v-1);
		tree[v-1].push_back(u-1);

	}


    in >> cc;

    for (i=0;i<cc;i++)
    {
        in >> u >> v;
        //printf("  %d  ~~~ %d  ",u,v);
        a=lca(u-1,v-1,&first_entrance,   &table,&ord,&h    );
        out<<1+a<<endl;

    }

	in.close();
	out.close();
	return 0;
}
