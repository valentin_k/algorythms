#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

using namespace std;

struct node
{
    string s;
    struct node * left;
    struct node * right;
    struct node * up;
};

bool compare (string a, string b)
{

  int i=0;

  while ((i<a.length()) && (i<b.length()))
  {
    if (a[i] < b[i])
    {
       return true;
    }
    else if (a[i]>b[i])
    {
        return false;
    }
    i++;
  }

  if (a.length() < b.length())
  {
      return true;
  }
  else
  {
      return false;
  }


  return (a<=b);
}

void  add(node * root, string s)
{
    bool flag= true;
    struct node * current;
    if (root->up==(struct node *)1)
    {
        root->s=s;
        root->up=0;
    }
    else
    {
        current=root;
        while (flag)
        {
            if (compare(s,current->s))
            {
                if(current->right==0)
                {
                    current->right=new(struct node);
                    current->right->s=s;
                    current->right->up=current;
                    current->right->left=0;
                    current->right->right=0;
                    flag=false;
                }
                else
                {
                    current=current->right;
                }
            }
            else
            {
                if(current->left==0)
                {
                    current->left=new(struct node);
                    current->left->s=s;
                    current->left->up=current;
                    current->left->left=0;
                    current->left->right=0;
                    flag=false;
                }
                else
                {
                    current=current->left;
                }
            }
        }
    }
}

void del(node * root, string s)
{
    int i;
    bool flag= true;
    struct node * current;
    struct node * leftest;
    string target;

    if (root->up==(struct node *)1)
    {
    }
    else
    {
        current=root;
        i=0;
        while (flag)
        {
            if(current->s==s)
            {
                    if (current->left==0&&current->right==0)
                    {
                       if (current->up==0)
                       {
                           current->up=(struct node *)1;
                           current->left=0;
                           current->right=0;
                       }
                       else
                       {
                            if (current->up->right==current)
                            {
                                current->up->right=0;
                            }
                            else
                            {
                                current->up->left=0;
                            }
                            free(current);
                       }

                    }
                    else if (current->left==0)
                    {
                        current->right->up=current->up;

                        if (current->up->right==current)
                        {
                            current->up->right=current->right;
                        }
                        else
                        {
                            current->up->left=current->right;
                        }
                        free(current);
                    }
                    else if (current->right==0)
                    {
                        current->left->up=current->up;

                        if (current->up->right==current)
                        {
                            current->up->right=current->left;
                        }
                        else
                        {
                            current->up->left=current->left;
                        }
                        free(current);
                    }
                    else
                    {
                        leftest=current->right;
                        while(flag)
                        {
                            if (leftest->left==0)
                            {
                                target=leftest->s;
                                del(root,target);
                                flag=false;
                            }
                            else
                            {
                                leftest=current->left;
                            }
                        }
                        current->s=target;
                    }

                    flag=false;
            }
            else
            {
                if (compare(s,current->s))
                {
                    if(current->right==0)
                    {
                        flag=false;
                    }
                    else
                    {
                        current=current->right;
                    }
                }
                else
                {
                    if(current->left==0)
                    {
                        flag=false;
                    }
                    else
                    {
                        current=current->left;
                    }
                }
            }
        }
    }
}

void find (node * root, string s)
{
    int i;
    bool flag= true;
    struct node * current;
    if (root->up==(struct node *)1)
    {
        cout<<"n";
    }
    else
    {
        current=root;
        i=0;
        while (flag)
        {
            if(current->s==s)
            {
                    cout<<i;
                    //cout<<current->up->s<<" "<<current->left->s<<" "<<current->right->s<<" ";
                    flag=false;
            }
            else
            {
                if (compare(s,current->s))
                {
                    if(current->right==0)
                    {
                        cout<<"n";
                        flag=false;
                    }
                    else
                    {
                        current=current->right;
                    }
                }
                else
                {
                    if(current->left==0)
                    {
                        cout<<"n";
                        flag=false;
                    }
                    else
                    {
                        current=current->left;
                    }
                }
            }
            i++;
        }
    }
}

void output (node * root, int level,char dir )
{

        struct node * current;

        current=root;
        //cout<<current->s<<"  , level "<<level<<"  dir "<<dir<<endl;

        if (current->left!=0)
        {
            output(current->left,level+1,'l');
        }
        if (current->right!=0)
        {
            output(current->right,level+1,'r');
        }

}


int main()
{

    string s_i;
    string s;
    string command;

    struct node *tree;

    tree=new(struct node);

    tree->up=(struct node *)1;
    tree->left=0;
    tree->right=0;

    //---------------------------------------------------------------------------


    freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);



    //---------------------------------------------------------------------------

    while (true)
    {       s.clear();
            command.clear();
            cin>>s_i;
            s.insert(0,s_i, 1, s_i.length()-1);
            command.insert(0,s_i, 0, 1);

            if (command=="E")
            {
                break;
            }
            else if(command=="+")
            {
                add(tree,s);
            }
            else if(command=="-")
            {
                del(tree,s);
            }
            else if(command=="?")
            {
                find(tree,s);
            }


    }

    cout<<endl;
    output (tree, 0 ,'t');



    return 0;
}
